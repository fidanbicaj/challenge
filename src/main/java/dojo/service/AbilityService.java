package dojo.service;

import dojo.exception.AbilityException;
import dojo.model.Ability;
import dojo.model.Hero;
import dojo.repository.AbilityRepository;
import dojo.util.Constant;
import dojo.util.JsonReader;
import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

/**
 * Created by MT12 on 14/08/2018.
 */
@Service
@Transactional
public class AbilityService {

    @Autowired
    private AbilityRepository abilityRepository;

    @Autowired
    private HeroService heroService;

    public List<Ability> findAll(){
        return abilityRepository.findAll();
    }

    public Ability findById(Integer id){
        Optional<Ability> abilityOptional = abilityRepository.findById( id);
        if(abilityOptional.isPresent()){
            return abilityOptional.get();
        }else{
            throw new AbilityException("Ability with id :"+id+" doesn't exist");
        }

    }

    public void syncAbilities() throws Exception{
        List<Ability> apiAbilityList = this.getApiAbilityList();
        List<Ability> dbAbilityList = this.abilityRepository.findAll();
        apiAbilityList.removeAll(dbAbilityList);
        this.abilityRepository.saveAll(apiAbilityList);
    }
    private String getAbilityURL() throws Exception{
        StringBuilder sb = new StringBuilder();
        sb.append(Constant.API_URL);
        sb.append("/ability/");
        return sb.toString();
    }

    public List<Ability> getApiAbilityList() throws Exception{
        List<Ability> abilityList = new ArrayList<>();
        JSONObject jsonObject = JsonReader.readJsonFromUrl(this.getAbilityURL());
        JSONArray jsonArray = jsonObject.getJSONArray("data");
        for (int i = 0 ; i < jsonArray.length(); i++){
            JSONObject obj = jsonArray.getJSONObject( i );
            Ability ability = new Ability();

            ability.setName( obj.getString("name") );
            ability.setDescription( obj.getString("description") );
            ability.setIsUltimate( obj.getBoolean("is_ultimate") );
            ability.setHeroId( this.reconstituteHero( obj.getJSONObject("hero") ) );

            abilityList.add(ability);
        }
        return abilityList;
    }

    private Hero reconstituteHero(JSONObject heroJsonObject) throws  Exception{
        return heroService.findByName( heroJsonObject.getString("name") );
    }


}
