package dojo.service;

import dojo.exception.HeroException;
import dojo.model.Hero;
import dojo.repository.HeroRepository;
import dojo.util.Constant;
import dojo.util.JsonReader;
import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;


/**
 * Created by MT12 on 14/08/2018.
 */
@Service
@Transactional
public class HeroService {

    @Autowired
    private HeroRepository heroRepository;

    public List<Hero> findAll(){
        return heroRepository.findAll();
    }

    public Hero findById(Integer id) {
        Optional<Hero> heroOptional = heroRepository.findById(id);
        if(heroOptional.isPresent()){
            return heroOptional.get();
        }else{
            throw new HeroException("Ability with id :"+id+" doesn't exist");
        }
    }

    public Hero findByName(String name){
        return heroRepository.findByName( name );
    }

    public void syncHeroes() throws Exception {
        List<Hero> apiHeroList = this.getApiHeroeList();
        List<Hero> dbHeroList = this.heroRepository.findAll();
        apiHeroList.removeAll(dbHeroList);
        heroRepository.saveAll(apiHeroList);

    }

    private String getHeroURL(){
        StringBuilder sb = new StringBuilder();
        sb.append(Constant.API_URL);
        sb.append("/hero/");
        return sb.toString();
    }

    private List<Hero> getApiHeroeList() throws Exception{
        List<Hero> heroList = new ArrayList<>();
        JSONObject jsonObject = JsonReader.readJsonFromUrl(this.getHeroURL());
        JSONArray jsonArray = jsonObject.getJSONArray("data");
        for (int i = 0 ; i < jsonArray.length(); i++){
            Hero hero = new Hero();
            JSONObject obj = jsonArray.getJSONObject( i );

            hero.setName( obj.getString("name"));
            hero.setRealName( obj.getString("real_name"));
            hero.setArmour( obj.getInt("armour"));
            hero.setShield( obj.getInt("shield"));
            hero.setHealth( obj.getInt("health"));

            heroList.add(hero);
        }
        return heroList;
    }

}
