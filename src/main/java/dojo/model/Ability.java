package dojo.model;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;


/**
 * Created by MT12 on 14/08/2018.
 */
@Entity
@Table(name = "ability")
@XmlRootElement
@NamedQueries({
        @NamedQuery(name = "Ability.findAll", query = "SELECT a FROM Ability a")
        , @NamedQuery(name = "Ability.findById", query = "SELECT a FROM Ability a WHERE a.id = :id")
        , @NamedQuery(name = "Ability.findByName", query = "SELECT a FROM Ability a WHERE a.name = :name")
        , @NamedQuery(name = "Ability.findByIsUltimate", query = "SELECT a FROM Ability a WHERE a.isUltimate = :isUltimate")})
public class Ability implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;
    @Size(max = 45)
    @Column(name = "name")
    private String name;
    @Lob
    @Size(max = 2147483647)
    @Column(name = "description")
    private String description;
    @Column(name = "is_ultimate")
    private Boolean isUltimate;
    @JoinColumn(name = "hero_id", referencedColumnName = "id")
    @ManyToOne
    private Hero heroId;

    public Ability() {
    }

    public Ability(Integer id) {
        this.id = id;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public boolean getIsUltimate() {
        return isUltimate;
    }

    public void setIsUltimate(boolean isUltimate) {
        this.isUltimate = isUltimate;
    }

    public Hero getHeroId() {
        return heroId;
    }

    public void setHeroId(Hero heroId) {
        this.heroId = heroId;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Ability ability = (Ability) o;

        if (name != null ? !name.equals(ability.name) : ability.name != null) return false;
        return heroId != null ? heroId.equals(ability.heroId) : ability.heroId == null;
    }

    @Override
    public String toString() {
        return "Ability{" +
                "name='" + name + '\'' +
                '}';
    }
}
