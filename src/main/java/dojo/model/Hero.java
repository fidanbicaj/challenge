package dojo.model;

import com.fasterxml.jackson.annotation.JsonIgnore;

import java.io.Serializable;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;


/**
 * Created by MT12 on 14/08/2018.
 */
@Entity
@Table(name = "hero")
@XmlRootElement
@NamedQueries({
        @NamedQuery(name = "Hero.findAll", query = "SELECT h FROM Hero h")
        , @NamedQuery(name = "Hero.findById", query = "SELECT h FROM Hero h WHERE h.id = :id")
        , @NamedQuery(name = "Hero.findByName", query = "SELECT h FROM Hero h WHERE h.name = :name")
        , @NamedQuery(name = "Hero.findByRealName", query = "SELECT h FROM Hero h WHERE h.realName = :realName")
        , @NamedQuery(name = "Hero.findByHealth", query = "SELECT h FROM Hero h WHERE h.health = :health")
        , @NamedQuery(name = "Hero.findByArmour", query = "SELECT h FROM Hero h WHERE h.armour = :armour")
        , @NamedQuery(name = "Hero.findByShield", query = "SELECT h FROM Hero h WHERE h.shield = :shield")})
public class Hero implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;
    @Size(max = 45)
    @Column(name = "name")
    private String name;
    @Size(max = 45)
    @Column(name = "real_name")
    private String realName;
    @Column(name = "health")
    private Integer health;
    @Column(name = "armour")
    private Integer armour;
    @Column(name = "shield")
    private Integer shield;
    @OneToMany(mappedBy = "heroId")
    @JsonIgnore
    private List<Ability> abilityList;

    public Hero() {
    }

    public Hero(Integer id) {
        this.id = id;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getRealName() {
        return realName;
    }

    public void setRealName(String realName) {
        this.realName = realName;
    }

    public Integer getHealth() {
        return health;
    }

    public void setHealth(Integer health) {
        this.health = health;
    }

    public Integer getArmour() {
        return armour;
    }

    public void setArmour(Integer armour) {
        this.armour = armour;
    }

    public Integer getShield() {
        return shield;
    }

    public void setShield(Integer shield) {
        this.shield = shield;
    }

    @XmlTransient
    public List<Ability> getAbilityList() {
        return abilityList;
    }

    public void setAbilityList(List<Ability> abilityList) {
        this.abilityList = abilityList;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Hero hero = (Hero) o;

        return name != null ? name.equals(hero.name) : hero.name == null;
    }

    @Override
    public String toString() {
        return "Hero{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", realName='" + realName + '\'' +
                ", health=" + health +
                ", armour=" + armour +
                ", shield=" + shield +
                ", abilityList=" + abilityList +
                '}';
    }
}