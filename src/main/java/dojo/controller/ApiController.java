package dojo.controller;

import dojo.model.Ability;
import dojo.model.Hero;
import dojo.service.AbilityService;
import dojo.service.HeroService;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * Created by MT12 on 14/08/2018.
 */
@RestController
@RequestMapping("/api/")
public class ApiController {

    @Autowired
    private HeroService heroService;

    @Autowired
    private AbilityService abilityService;

    @GetMapping(value = "/hero/")
    @ApiOperation(value = "Finds all heroes from the database",
            notes = "Also synchronises all the hero data from the main API to the database ")
    public ResponseEntity getAllHeroes() throws Exception{
        heroService.syncHeroes();
        return new ResponseEntity<List<Hero>>(heroService.findAll(), HttpStatus.OK);
    }

    @ApiOperation(value = "Finds a Hero by its id",
            notes = "Also synchronises all the hero data from the main API to the database ")
    @GetMapping(value = "/hero/{id}")
    public ResponseEntity getHero(@PathVariable("id") Integer id) throws Exception{
        heroService.syncHeroes();
        return new ResponseEntity<Hero>( heroService.findById(id), HttpStatus.OK);
    }

    @ApiOperation(value = "Finds all Abilities from the database",
            notes = "Also synchronises all the ability data from the main API to the database ")
    @GetMapping(value = "/ability/")
    public ResponseEntity getAllAbilities() throws Exception{
        abilityService.syncAbilities();
        return new ResponseEntity<List<Ability>>(abilityService.findAll(), HttpStatus.OK);
    }

    @ApiOperation(value = "Finds the Ability by its id",
            notes = "Also synchronises all the ability data from the main API to the database ")
    @GetMapping(value = "/ability/{id}")
    public ResponseEntity getAbility(@PathVariable("id") Integer id) throws Exception{
        abilityService.syncAbilities();
        return new ResponseEntity<Ability>(abilityService.findById(id), HttpStatus.OK);
    }

}
