package dojo.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

/**
 * Created by MT12 on 14/08/2018.
 */
@ResponseStatus(HttpStatus.NOT_FOUND)
public class AbilityException extends RuntimeException {

    public AbilityException(String message) {
        super(message);
    }
}
