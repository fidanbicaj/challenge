package dojo.repository;

import dojo.model.Hero;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import java.util.List;

/**
 * Created by MT12 on 14/08/2018.
 */
public interface HeroRepository extends CrudRepository< Hero, Integer> {

    List<Hero> findAll();

    @Query("SELECT  h from Hero h where h.name = :name")
    Hero findByName(@Param("name") String name);
}
