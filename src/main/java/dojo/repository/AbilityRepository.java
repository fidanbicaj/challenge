package dojo.repository;

import dojo.model.Ability;
import dojo.model.Hero;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

/**
 * Created by MT12 on 14/08/2018.
 */
public interface AbilityRepository extends CrudRepository<Ability, Integer> {

    List<Ability> findAll();
}
