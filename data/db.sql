-- MySQL dump 10.13  Distrib 5.7.17, for Win64 (x86_64)
--
-- Host: localhost    Database: code_challenge
-- ------------------------------------------------------
-- Server version	5.7.14

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `ability`
--

DROP TABLE IF EXISTS `ability`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ability` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(45) DEFAULT NULL,
  `description` longtext,
  `is_ultimate` tinyint(4) DEFAULT NULL,
  `hero_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `ability_hero_fk_idx` (`hero_id`),
  CONSTRAINT `ability_hero_fk` FOREIGN KEY (`hero_id`) REFERENCES `hero` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=151 DEFAULT CHARSET=latin1 COMMENT='	';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ability`
--

LOCK TABLES `ability` WRITE;
/*!40000 ALTER TABLE `ability` DISABLE KEYS */;
INSERT INTO `ability` VALUES (101,'Biotic Rifle','Ana’s rifle shoots darts that can restore health to her allies or deal ongoing damage to her enemies. She can use the rifle’s scope to zoom in on targets and make highly accurate shots.',0,23),(102,'Sleep Dart','Ana fires a dart from her sidearm, rendering an enemy unconscious (though any damage will rouse them).',0,23),(103,'Biotic Grenade','Ana tosses a biotic bomb that deals damage to enemies and heals allies in a small area of effect. Affected allies briefly receive increased healing from all sources, while enemies caught in the blast cannot be healed for a few moments.',0,23),(104,'Nano Boost','After Ana hits one of her allies with a combat boost, they temporarily move faster, deal more damage, and take less damage from enemies’ attacks.',1,23),(105,'Configuration: Recon','In Recon mode, Bastion is fully mobile, outfitted with a submachine gun that fires steady bursts of bullets at medium range.',0,24),(106,'Configuration: Sentry','In Sentry mode, Bastion is a stationary powerhouse equipped with a gatling gun capable of unleashing a hail of bullets. The gun\'s aim can be \"walked\" across multiple targets dealing devastating damage at short to medium range.',0,24),(107,'Reconfigure','Bastion transforms between its two primary combat modes to adapt to changing battlefield conditions.',0,24),(108,'Self-Repair','Bastion restores its health; it cannot move or fire weapons while the repair process is in effect.',0,24),(109,'Configuration: Tank','In Tank mode, Bastion extends wheeled treads and a powerful long-range cannon. The cannon’s explosive shells demolish targets in a wide blast radius, but Bastion can only remain in this mode for a limited time.',1,24),(110,'Fusion Cannons','D.Va\'s mech is equipped with twin short-range rotating cannons. They lay down continuous, high-damage fire without needing to reload, but slow D.Va’s movement while they’re active.',0,25),(111,'Boosters','D.Va’s mech launches into the air, her momentum carrying her forward. She can turn and change directions or barrel through her enemies, knocking them back.',0,25),(112,'Defense Matrix','D.Va can activate this forward-facing targeting array to shoot incoming projectiles out of the air.',0,25),(113,'Eject','D.Va ejects out of her mech when it is destroyed.',0,25),(114,'Self-Destruct','D.Va ejects from her mech and sets its reactor to explode, dealing massive damage to nearby opponents.',1,25),(115,'Light Gun','While outside of her mech, D.Va can continue the fight with a mid-range automatic blaster.',0,25),(116,'Call Mech','If her armored battle suit is destroyed, D.Va can call down a fresh mech and return to the fray.',1,25),(117,'Shuriken','Genji looses three deadly throwing stars in quick succession. Alternatively, he can throw three shuriken in a wider spread.',0,26),(118,'Swift Strike','Genji darts forward, slashing with his katana and passing through foes in his path. If Genji eliminates a target, he can instantly use this ability again.',0,26),(119,'Deflect','With lightning-quick swipes of his sword, Genji reflects any oncoming projectiles and can send them rebounding towards his enemies.',0,26),(120,'Cyber-Agility','Thanks to his cybernetic abilities, Genji can climb walls and perform jumps in mid-air.',0,26),(121,'Dragonblade','Genji brandishes his katana for a brief period of time. Until he sheathes his sword, Genji can deliver killing strikes to any targets within his reach.',1,26),(122,'Storm Bow','Hanzo nocks and fires an arrow at his target.',0,27),(123,'Scatter Arrow','Hanzo shoots a fragmenting arrow that ricochets off walls and objects and can strike multiple targets at once.',0,27),(124,'Sonic Arrow','Hanzo launches an arrow that contains a sonar tracking device. Any enemy within its detection radius is visibly marked, making them easier for Hanzo and his allies to hunt down.',0,27),(125,'Wall Climb','Hanzo can climb any vertical surface.',0,27),(126,'Dragonstrike','Hanzo summons a Spirit Dragon which travels through the air in a line. It passes through walls in its way, devouring any enemies it encounters.',1,27),(127,'Frag Launcher','Junkrat\'s Frag Launcher lobs grenades a significant distance. They bounce to reach their destination, and blow up when they strike an enemy.',0,28),(128,'Concussion Mine','After placing one of his homemade Concussion Mines, Junkrat can trigger it to damage enemies and send them flying... or propel himself through the air.',0,28),(129,'Steel Trap','Junkrat tosses out a giant, metal-toothed trap. Should an enemy wander too close to the trap, it clamps on, injuring and immobilizing them.',0,28),(130,'Total Mayhem','Junkrat\'s deranged sense of humor persists past his death. If killed, he drops several live grenades.',0,28),(131,'RIP-Tire','Junkrat revs up a motorized tire bomb and sends it rolling across the battlefield, climbing over walls and obstacles. He can remotely detonate the RIP-Tire to deal serious damage to enemies caught within the blast, or just wait for it to explode on its own.',1,28),(132,'Sonic Amplifier','Lúcio can hit his enemies with sonic projectiles or knock them back with a blast of sound.',0,29),(133,'Crossfade','Lúcio continuously energizes himself, and nearby teammates, with music. He can switch between two songs: one amplifies movement speed, while the other regenerates health.',0,29),(134,'Amp It Up','Lúcio increases the volume on his speakers, boosting the effects of his songs.',0,29),(135,'Wall Ride','Lúcio rides along a wall. This has a slight upwards angle, allowing him to ascend the wall.',0,29),(136,'Sound Barrier','Protective waves radiate out from Lúcio’s Sonic Amplifier, briefly providing him and nearby allies with personal shields.',1,29),(137,'Peacekeeper','McCree fires off a round from his trusty six-shooter. He can fan the Peacekeeper’s hammer to swiftly unload the entire cylinder.',0,30),(138,'Combat Roll','McCree dives in the direction he\'s moving, effortlessly reloading his Peacekeeper in the process.',0,30),(139,'Flashbang','McCree heaves a blinding grenade that explodes shortly after it leaves his hand. The blast staggers enemies in a small radius.',0,30),(140,'Deadeye','Focus. Mark. Draw. McCree takes a few precious moments to aim; when he\'s ready to fire, he shoots every enemy in his line of sight. The weaker his targets are, the faster he\'ll line up a killshot.',1,30),(141,'Endothermic Blaster','Mei\'s blaster unleashes a concentrated, short-range stream of frost that damages, slows, and ultimately freezes enemies in place. Mei can also use her blaster to shoot icicle-like projectiles at medium range.',0,31),(142,'Cryo-Freeze','Mei instantly surrounds herself with a block of thick ice. She heals and ignores damage while encased, but cannot move or use abilities.',0,31),(143,'Ice Wall','Mei generates an enormous ice wall that obstructs lines of sight, stops movement, and blocks attacks.',0,31),(144,'Blizzard','Mei deploys a weather-modification drone that emits gusts of wind and snow in a wide area. Enemies caught in the blizzard are slowed and take damage; those who linger too long are frozen solid.',1,31),(145,'Caduceus Staff','Mercy engages one of two beams that connect to an ally. By maintaining the beams, she can either restore that ally\'s health or increase the amount of damage they deal.',0,32),(146,'Caduceus Blaster','Mercy shoots a round from her sidearm. It\'s best reserved for emergency personal defense.',0,32),(147,'Guardian Angel','Mercy flies towards a targeted ally, allowing her to reach them quickly and provide assistance in crucial moments.',0,32),(148,'Angelic Descent','Propelled by her Valkyrie suit, Mercy slows the speed of her descent from great heights.',0,32),(149,'Resurrect','Mercy radiates restorative power, bringing nearby dead allies back into the fight with full health.',1,32),(150,'Rocket Launcher','Pharah’s primary weapon launches rockets that deal significant damage in a wide blast radius.',0,33);
/*!40000 ALTER TABLE `ability` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `hero`
--

DROP TABLE IF EXISTS `hero`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `hero` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(45) CHARACTER SET latin1 DEFAULT NULL,
  `real_name` varchar(45) DEFAULT NULL,
  `health` int(32) DEFAULT NULL,
  `armour` int(32) DEFAULT NULL,
  `shield` int(32) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=47 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `hero`
--

LOCK TABLES `hero` WRITE;
/*!40000 ALTER TABLE `hero` DISABLE KEYS */;
INSERT INTO `hero` VALUES (23,'Ana','Ana Amari',200,0,0),(24,'Bastion','SST Laboratories Siege Automaton E54',200,100,0),(25,'D.Va','Hana Song',200,400,0),(26,'Genji','Genji Shimada',200,0,0),(27,'Hanzo','Hanzo Shimada',200,0,0),(28,'Junkrat','Jamison Fawkes',200,0,0),(29,'Lúcio','Lúcio Correia dos Santos',200,0,0),(30,'McCree','Jesse McCree',200,0,0),(31,'Mei','Mei-Link Zhou',250,0,0),(32,'Mercy','Angela Ziegler',200,0,0),(33,'Pharah','Fareeha Amari',200,0,0),(34,'Reaper','Gabriel Reyes',250,0,0),(35,'Reinhardt','Reinhardt Wilhelm',300,200,0),(36,'Roadhog','Mako Rutledge',600,0,0),(37,'Soldier: 76','Jack Morrison',200,0,0),(38,'Symmetra','Satya Vaswani',100,0,0),(39,'Torbjörn','Torbjörn Lindholm',200,0,0),(40,'Tracer','Lena Oxton',150,0,0),(41,'Widowmaker','Amélie Lacroix',200,0,0),(42,'Winston','Winston',400,100,0),(43,'Zarya','Aleksandra Zaryanova',200,0,200),(44,'Zenyatta','Tekhartha Zenyatta',50,0,150),(45,'Sombra','░░░░░░',200,0,0),(46,'Orisa','Orisa',200,200,0);
/*!40000 ALTER TABLE `hero` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2018-08-14 17:12:51
